package test.com.yt.dao;

import com.yt.dao.GuoDao;
import com.yt.dao.ShiDao;
import com.yt.entity.Guo;
import com.yt.entity.Sheng;
import com.yt.entity.Shi;
import com.yt.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @Classname ShiDaoTest
 * @Description
 * @Date 2021/11/1 10:21
 * @Create by 杨涛
 */
public class GuoDaoTest {

    @Test
    public void testgetGuos(){
       List<Guo> guos=guoDao.getGuos();
        System.out.println(guos.size());
       for(Guo g:guos){
           System.out.println(g.getId()+g.getName());
           System.out.println("包含省：");
           for(Sheng s:g.getShengs()){
               System.out.println(s);
           }
       }
    }

    SqlSession session=null;
    GuoDao guoDao=null;
    @Before
    public void init(){
        session =  MyBatisUtil.createSqlSession();

        guoDao=session.getMapper(GuoDao.class);
    }
    @After
    public void end(){
        session.commit();
        MyBatisUtil.closeSqlSession(session);
    }
}
