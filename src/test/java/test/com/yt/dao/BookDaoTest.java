package test.com.yt.dao;

import com.yt.dao.BookDao;
import com.yt.dao.GuoDao;
import com.yt.entity.Book;
import com.yt.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

/**
 * @Classname BookDaoTest
 * @Description
 * @Date 2021/11/2 14:21
 * @Create by 杨涛
 */

public class BookDaoTest {
    @Test
    public void booktest(){
        Book book=new Book();
        book.setBookname("java");
        if( book.getBid()!=null )
            System.out.println("id:"+book.getBid());
        if(book.getBookname()!=null)
            System.out.println("bookname:"+book.getBookname());
        System.out.println("over");
    }

    @Test
    public void testgetBook(){
        Book book1=new Book();
        book1.setPublish("人民邮电出版社");
        book1.setAuthor("贾鹏博");
        book1.setVersion("2.0");
        List<Book> books=bookDao.getBooksByBook(book1);
        for(Book book:books)
            System.out.println(book);
    }

    @Test
    public void testaddBook() {
        Book[] books = new Book[]{
//                new Book(0,"java","人民邮电出版社",
//                        "贾鹏博","1.0",new Date(),23.4f),
//                new Book(0,"mysql","人民邮电出版社",
//                        "杨涛","1.0",new Date(),99.9f),
//                new Book(0,"mysql","人民邮电出版社",
//                        "杨涛","2.0",new Date(),99.9f),
//                new Book(0,"mysql","人民邮电出版社",
//                        "贾鹏博","2.0",new Date(),99.9f),
//                new Book(0,"java web","人民邮电出版社",
//                        "郑毛强","1.0",new Date(),69.9f),
//                new Book(0,"java web","人民邮电出版社",
//                        "杨涛","1.0",new Date(),119.9f),
//                new Book(0,"ssm","清华大学出版社",
//                        "杨涛","2.0",new Date(),599.9f),
                new Book(0,"spring boot","清华大学出版社",
                        "杨涛","1.0",new Date(),389.9f)
        };
        for (Book book : books)
            bookDao.addBook(book);
    }

    SqlSession session = null;
    BookDao bookDao = null;

    @Before
    public void init() {
        session = MyBatisUtil.createSqlSession();

        bookDao = session.getMapper(BookDao.class);
    }

    @After
    public void end() {
        session.commit();
        MyBatisUtil.closeSqlSession(session);
    }
}
