package test.com.yt.dao;

import com.yt.dao.UserDao;
import com.yt.entity.User;
import com.yt.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Classname UserDao
 * @Description
 * @Date 2021/11/1 8:43
 * @Create by 杨涛
 */
public class UserDaoTest {
    SqlSession session=null;
    @Before
    public void init(){
        session =  MyBatisUtil.createSqlSession();
    }
    @After
    public void end(){
        session.commit();
        MyBatisUtil.closeSqlSession(session);
    }
    @Test
    public void getAllUsers() {

//        SqlSession session = MyBatisUtil.createSqlSession();
        List<User> users=session.getMapper(UserDao.class).getAllUsers();
        for(User u:users){
            System.out.println(u);
        }


    }

    @Test
    public void getUsersByName() {
        List<User> userlist = session.getMapper(UserDao.class).getUsersByName("人");
        for (User u : userlist) {
            System.out.println(u);
        }
    }

    @Test
    public void login () {
        User u = new User();
        u.setUsername("王宇翔");
        u.setPassword("123");
        u = session.getMapper(UserDao.class).login(u);

        System.out.println(u);
    }


    @Test
    public void loginByMap( ) {
        Map map=new HashMap<String,String>();
        map.put("name","admin");
        map.put("password","admin");
        User u = session.getMapper(UserDao.class).loginByMap(map );

        System.out.println(u);
    }


    @Test
    public void loginBynameAndpwd( ) {
        User u = session.getMapper(UserDao.class)
                .loginBynameAndpwd("name2","123" );

        System.out.println(u);
    }


    @Test
    public void addUser( ) {
        UserDao userDao=session.getMapper(UserDao.class);
        User u=new User();
        u.setUsername("wangzeng1");
        u.setPassword("000000");
        u.setNickname("王增");
        u.setLevel(100);
        int line  = userDao.addUser(u) ;
        System.out.println("影响了：" +
                line+"行");
    }


    @Test
    public  void updateUser( ) {
        UserDao userDao=session.getMapper(UserDao.class);
        User u=new User();
        u.setId(16);
        u.setUsername("wangzeng2");
        u.setPassword("111111");
        u.setNickname("王增2");
        u.setLevel(99);
        int line  = userDao.updateUser( u) ;
        System.out.println("影响了：" +
                line+"行");
    }
}
