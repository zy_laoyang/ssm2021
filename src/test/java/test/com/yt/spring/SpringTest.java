package test.com.yt.spring;

import com.yt.aop.MyClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Classname SpringTest
 * @Description
 * @Date 2021/11/3 8:52
 * @Create by 杨涛
 */
public class SpringTest {
    @Test
    public void test(){
        // 通过ClassPathXmlApplicationContext实例化Spring的上下文
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "app.xml");
        MyClass mc= (MyClass) context.getBean("mc");
                //context.getBean("com.yt.aop.Student");
        System.out.println(mc);
    }
}
