package com.yt.entity;

import org.apache.ibatis.type.Alias;

import java.util.Date;
@Alias("u")
public class User {
    private int id;
    private String username;
    private String password;
    private String nickname;
    private Date regdate;
    private int level;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public Date getRegdate() {
        return regdate;
    }
    public void setRegdate(Date regdate) {
        this.regdate = regdate;
    }
    public int getLevel() {
        return level;
    }
    public void setLevel(int level) {
        this.level = level;
    }
    @Override
    public String toString() {
        return "User [id=" + id + ", username=" + username + ", password=" + password + ", nickname=" + nickname
                + ", regdate=" + regdate + ", level=" + level + "]";
    }
    public User() {}

    public User(int id, String username, String password, String nickname, Date regdate, int level) {

        this.id = id;
        this.username = username;
        this.password = password;
        this.nickname = nickname;
        this.regdate = regdate;
        this.level = level;
    }




}
