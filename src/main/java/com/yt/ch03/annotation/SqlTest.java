package com.yt.ch03.annotation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SqlTest {
	public static void main(String[] args) {
		BaseDao bd=new BaseDao();
		Class bdc=bd.getClass();
		Field [] fs=bdc.getDeclaredFields();
		for(Field f:fs){
			System.out.println(f);
			Sql sql=f.getAnnotation(Sql.class);
			if(sql !=null){
				System.out.println(sql.url());
				try {
					f.setAccessible(true);
					f.set(bd, sql.url());
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			
		}String res=null;
		Method [] ms=bdc.getDeclaredMethods();
		for(Method m:ms){
			System.out.println(m.getName());
			if(m.getName().equals("toString")){
				try {
					res=(String) m.invoke(bd, null);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.println(res);
	}
}
