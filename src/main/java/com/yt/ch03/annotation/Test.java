package com.yt.ch03.annotation;

import java.lang.reflect.Method;

public class Test {
    public static void main(String[] args) {
        try {
            Student student = Student.class.newInstance();
            Method [] ms =Student.class.getDeclaredMethods();
            for (Method m:ms){
                System.out.println(m.getName());
                if(m.getName().equals("show1"))
                    m.invoke(student);
                else
                    m.invoke(student,21);
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}
