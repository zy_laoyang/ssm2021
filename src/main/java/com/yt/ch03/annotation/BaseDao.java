package com.yt.ch03.annotation;

public class BaseDao {
	@Sql(url="http://127.0.0.1")
	private String url;
	public String toString(){
		return url;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
