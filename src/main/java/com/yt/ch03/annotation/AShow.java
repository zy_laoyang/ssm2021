package com.yt.ch03.annotation;
/**
 * 注解的使用
 * @author laoyang
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 声明注解的保留期
 * RUNTIME 保留至运行时
 * CLASS 保留至class文件中,运行时将不带入jvm
 * SOURCE 仅保留在源码中,编译期就不保留
 */
@Retention(RetentionPolicy.RUNTIME)
/**
 * 声明可以使用这个注解的目标的类型
 * TYPE	类 接口 注解类 enum
 * FIELD (类的成员)变量或常量声明处
 * METHOD 方法声明处
 * PARAMETER 参数
 * CONSTRUCTOR	构造
 * LOCAL_VARIABLE 局部变量
 * ANNOTATION_TYPE 注解类的注解
 * PACKAGE 包声明处
 */
@Target(ElementType.METHOD)
public @interface AShow {//声明一个注解
	boolean value();//注解的成员可以是多个
}















