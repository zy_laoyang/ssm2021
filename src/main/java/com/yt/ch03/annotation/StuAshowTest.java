package com.yt.ch03.annotation;

import java.lang.reflect.Method;


public class StuAshowTest {

	public static void main(String[] args) {
		Student student=new Student();
		Class stu=student.getClass();//class 对象是一个类的元数据
		Method [] methods=stu.getMethods();
		for(Method method:methods){
			AShow a=method.getAnnotation(AShow.class);
			int i=0;
			if(a!=null){
				System.out.print(method.getName()+"的注解值为:");
				System.out.println(a.value());
				if(a.value()){
					System.out.println("值为true");
					i=9;
				}else{
					System.out.println("值为false");
				}

			}
			try{
				if(method.getName().equals("show1"))
					method.invoke(student);
				else
					method.invoke(student,i);
			}catch (Exception e){

			}

		}
	}
}
