aop

Aspect oriented propgramming
-------------------------------------------------
代理模式
静态和动态两种
动态代理:DworkProxy
静态代理:SworkProxy
-------------------------------------------------

概念:
Cross-cutting (横切) concern(关注)
例如:
	开始和结束两个方法横切入系统
---------------------------
Aspect 方面
将上述cross-cutting concern收集起来,设计成各个独立可重用的对象就叫做Aspect

---------------------------
Weave 织入
在系统中,需要使用Aspect 时,将其Weave (织入)系统

---------------------------

Advice(增强)
Aspect 当中对Cross-cutting concerns 的具体实现称之为Advice
Advice中包括了Cross-cutting concerns 的行为或所要提供的服务
如:
	动态代理:DworkProxy
	静态代理:SworkProxy

---------------------------

JoinPoint
Advice 被执行的时机叫做 接入点
在spring 中只支持对于方法的JoinPoint
也就是说,接入点是某个方法的开始或结束时

---------------------------

PointCut
用来定义感兴趣的JoinPoint
可以定义文件或是Annotation 中编写Pointcut 用来说明Advice要应用在方法的前面还是后面或前后都有

---------------------------
Target(目标)
一个Advice 被应用的对象或是目标对象
比如:Worker
---------------------------
Introduction
对于一个现存的类,Introduction 可以为其增加行,且不用修改这个类的程序,具体的来说
可以为某个已经编译完的类,在执行期动态的加入一些方法或是行为
而不用修改或新增代码
---------------------------
proxy
代理

===================================================

execution(* com..*.*+(int))
表达式
用于描述方法
表达式的结构分为：
    访问修饰符 返回值 包名.包名.包名...类名.方法名(参数列表)
标准的表达式写法(下面举例说明，如果想精准的匹配到某个类的某个方法，表达式应该怎么写)：
    （无省略版写法：精准匹配到AccountServiceImpl类的saveAccount()方法）
    public void com.dongkuku.service.impl.AccountServiceImpl.saveAccount()
    访问修饰符可以省略
        void com.dongkuku.service.impl.AccountServiceImpl.saveAccount()
    返回值可以使用通配符 * ，表示任意返回值
        * com.dongkuku.service.impl.AccountServiceImpl.saveAccount()
    包名可以使用通配符 * ，表示任意包。但是有几级包，就需要写几个*
        (. 是包和包、包和类等之间的连接符)
        * *.*.*.*.AccountServiceImpl.saveAccount()
    包名可以使用 .. 表示当前包及其子包
        * *..AccountServiceImpl.saveAccount()
    类名和方法名都可以使用*来实现通配
        （这个精简版的表达式，表示匹配所有无参方法）
        * *..*.*()
    参数列表：
        可以直接写数据类型：
            基本类型直接写名称         int
            引用类型写包名.类名的方式   java.lang.String
        可以使用通配符 * 表示任意类型，但是必须有参数
        可以使用.. 表示有无参数均可，有参数可以是任意类型
    全通配写法：
        （全通匹配写法，表示匹配所有包下的所有类的所有方法）
        * *..*.*(..)

    实际开发中切入点表达式的通常写法：
        切到业务层实现类下的所有方法
              * com.dongkuku.service.impl.*.*(..)



