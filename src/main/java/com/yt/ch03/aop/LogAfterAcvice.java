package com.yt.ch03.aop;

import java.lang.reflect.Method;

import org.springframework.aop.AfterReturningAdvice;

public class LogAfterAcvice implements AfterReturningAdvice {
	public void afterReturning(Object arg0, Method arg1, Object[] arg2,
			Object arg3) throws Throwable {
		System.out.println(arg1.getName()+"....结束调用");
		
	}
}
