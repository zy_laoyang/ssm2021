package com.yt.ch03.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.After;
@Aspect
public class LogAuto {
	@Before("execution(* com.yt.ch03.entity..*.*(..))")
	public void before(JoinPoint joinPoint)
			throws Throwable {
		System.out.println("开始执行"+joinPoint.getSignature().getDeclaringTypeName()+"."
				+joinPoint.getSignature().getName());
	}
	@After("execution(* com.yt.ch03.entity..*.*(..))")
	public void after(JoinPoint joinPoint){
		System.out.println("结束执行"+joinPoint.getSignature().getDeclaringTypeName()+"."
				+joinPoint.getSignature().getName());

	}
}