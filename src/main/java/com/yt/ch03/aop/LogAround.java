package com.yt.ch03.aop;


import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class LogAround implements MethodInterceptor {
	public Object invoke(MethodInvocation methodi) throws Throwable {
		Object result = null;

		start();
		try{
		result = methodi.proceed();
		}finally{
			end();
		}
		return result;
	}
	public void start() {
		System.out.println("开始");
	}

	public void end() {
		System.out.println("结束");
	}

}
