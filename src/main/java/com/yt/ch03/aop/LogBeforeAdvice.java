package com.yt.ch03.aop;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

public class LogBeforeAdvice 
	implements MethodBeforeAdvice{
	public void before(Method method, Object[] arg1, Object arg2)
			throws Throwable {
		System.out.println("开始执行"+method.getName()+".... ");
	}
}
