package com.yt.ch03.util;

import com.yt.ch03.biz.DworkProxy;
import com.yt.ch03.biz.Work;
import com.yt.ch03.entity.Worker;

public class Dtest {
	public static void main(String[] args) {
		DworkProxy dp=new DworkProxy();
		Work w=(Work) dp.bind(new Worker());
		w.work("aaa");
		w.show("a");
	}
}
