package com.yt.ch03.util;

import com.yt.ch03.entity.Worker;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.yt.ch03.biz.Work;

public class AopTest {
	public static void main(String[] args) {

		ApplicationContext ac=new ClassPathXmlApplicationContext("com/yt/ch03/aop.xml");
		Work work=(Work) ac.getBean("worker");
		work.work("abc");
		work.show("haha");
	}
}
