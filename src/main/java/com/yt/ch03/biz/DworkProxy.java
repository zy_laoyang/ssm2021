package com.yt.ch03.biz;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class DworkProxy implements InvocationHandler {
	private Object delegate;

	public Object bind(Object delegate) {
		this.delegate = delegate;
		return Proxy.newProxyInstance(delegate.getClass().getClassLoader(),
				delegate.getClass().getInterfaces(), this);
	}

	public void start() {
		System.out.println("开始");
	}

	public void end() {
		System.out.println("结束");
	}

	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		Object result = null;
		
		start();
		try{
			result = method.invoke(delegate, args);
		}finally{
			end();
		}

		return result;
	}

}






