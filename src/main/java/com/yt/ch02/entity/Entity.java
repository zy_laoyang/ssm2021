package com.yt.ch02.entity;

import java.util.*;
public class Entity { 
    private String[] someStrArray; 
    private Object[] someObjArray; 
    private List someList; 
    private Map someMap; 
    Properties pp;
	public String[] getSomeStrArray() {
		return someStrArray;
	}
	public void setSomeStrArray(String[] someStrArray) {
		this.someStrArray = someStrArray;
	}
	public Object[] getSomeObjArray() {
		return someObjArray;
	}
	public void setSomeObjArray(Object[] someObjArray) {
		this.someObjArray = someObjArray;
	}
	public List getSomeList() {
		return someList;
	}
	public void setSomeList(List someList) {
		this.someList = someList;
	}
	public Map getSomeMap() {
		return someMap;
	}
	public void setSomeMap(Map someMap) {
		this.someMap = someMap;
	}
	public Properties getPp() {
		return pp;
	}
	public void setPp(Properties pp) {
		this.pp = pp;
	}
}
