package com.yt.ch02.entity;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class Test {
	public static void main(String[] args) {

		ApplicationContext ac=
			new ClassPathXmlApplicationContext("com/yt/ch02/entity/entity.xml");

		Entity entity=(Entity) ac.getBean("entity");
		System.out.println( entity.getSomeObjArray()[0]);
		System.out.println( entity.getSomeObjArray()[1]);
		
	}
}
