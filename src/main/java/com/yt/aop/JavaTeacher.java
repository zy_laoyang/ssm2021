package com.yt.aop;

/**
 * @Classname JavaTeacher
 * @Description
 * @Date 2021/11/3 8:24
 * @Create by 杨涛
 */
public class JavaTeacher implements Teacher{
    @Override
    public void teach() {
        System.out.println("上java课");
    }
}
