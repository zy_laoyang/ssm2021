package com.yt.aop;

/**
 * @Classname Teacher
 * @Description
 * @Date 2021/11/3 8:23
 * @Create by 杨涛
 */
public interface Teacher {
    void teach();
}
