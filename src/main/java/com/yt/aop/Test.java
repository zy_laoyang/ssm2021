package com.yt.aop;

/**
 * @Classname Test
 * @Description
 * @Date 2021/11/3 8:26
 * @Create by 杨涛
 */
public class Test {
    public static void main(String[] args) {
        Teacher t=new ProxyTeacher(new JavaTeacher());
        t.teach();
    }
}
