package com.yt.aop;

import lombok.Data;

/**
 * @Classname MyClass
 * @Description
 * @Date 2021/11/3 9:07
 * @Create by 杨涛
 */
@Data
public class MyClass {
    Student stu;
}
