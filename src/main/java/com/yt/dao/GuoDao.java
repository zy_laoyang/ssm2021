package com.yt.dao;

import com.yt.entity.Guo;

import java.util.List;

/**
 * @Classname GuoDao
 * @Description
 * @Date 2021/11/1 10:40
 * @Create by 杨涛
 */
public interface GuoDao {
    List<Guo> getGuos();
}
