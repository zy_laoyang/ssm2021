package com.yt.dao;

import com.yt.entity.Shi;

import java.util.List;

/**
 * @Classname ShiDao
 * @Description
 * @Date 2021/11/1 10:18
 * @Create by 杨涛
 */
public interface ShiDao {
    List<Shi> getAll();
}
