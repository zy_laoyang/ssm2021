package com.yt.ch01.type2.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.yt.ch01.type2.entity.Student;

public class Test {
	public static void main(String[] args) {
		
		Student s=new Student();
		System.out.println(s.getName());
		
		ApplicationContext ac=new ClassPathXmlApplicationContext("com/yt/ch01/type1/student.xml");
		Student stu=(Student) ac.getBean("student");
		System.out.println(stu.getName());
	}
}
