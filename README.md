# mybatis

## 搭建MyBatis开发环境

###  半自动化的ORM实现

orm 实体关系映射 框架

文档结构：

![image-20211030081308091](C:\Users\杨涛\AppData\Roaming\Typora\typora-user-images\image-20211030081308091.png)

- 下载mybatis-3.2.2.jar包并导入工程

  ```pom.xml
   <dependencies>
          <dependency>
              <groupId>org.mybatis</groupId>
              <artifactId>mybatis</artifactId>
              <version>3.5.2</version>
          </dependency>
          <dependency>
              <groupId>mysql</groupId>
              <artifactId>mysql-connector-java</artifactId>
              <version>8.0.16</version>
          </dependency>
      </dependencies>
  ```

  

- 编写MyBatis核心配置文件(configuration.xml)

  ```mybatis-config.xml
  <?xml version="1.0" encoding="UTF8" ?>
  <!DOCTYPE configuration
  PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
  "http://mybatis.org/dtd/mybatis-3-config.dtd">
  
  <!-- 通过这个配置文件完成mybatis与数据库的连接 -->
  <configuration>
  
  	<!-- 引入 database.properties 文件-->
  	<properties resource="database.properties"/>
  	<!-- 配置mybatis的log实现为LOG4J -->
  <!--	<settings>-->
  <!--		<setting name="logImpl" value="LOG4J" />-->
  <!--	</settings>-->
  	<!--类型别名-->  
  	<typeAliases>
  	 	<!-- <typeAlias alias="User" type="cn.smbms.pojo.User"/> -->
  	 	<package name="com.yt.entity"/>
  	</typeAliases>
  	
  	<environments default="development">
  		<environment id="development">
  			<!--配置事务管理，采用JDBC的事务管理  -->
  			<transactionManager type="JDBC"></transactionManager>
  			<!-- POOLED:mybatis自带的数据源，JNDI:基于tomcat的数据源 -->
  			<dataSource type="POOLED">
  				<property name="driver" value="${driver}"/>
  				<property name="url" value="${url}"/>
  				<property name="username" value="${user}"/>
  				<property name="password" value="${password}"/>
  			</dataSource>
  		</environment>
  	</environments>
  	
  	<!-- 将mapper文件加入到配置文件中 -->
  	<mappers>
  		<mapper resource="userM.xml"/>
  	</mappers>
  
  
  
  </configuration>
  
  ```

  

- 创建实体类-POJO

  ```User.java
  package com.yt.entity;
  
  import java.util.Date;
  
  public class User {
      private int id;
      private String username;
      private String password;
      private String nickname;
      private Date regdate;
      private int level;
      public int getId() {
          return id;
      }
      public void setId(int id) {
          this.id = id;
      }
      public String getUsername() {
          return username;
      }
      public void setUsername(String username) {
          this.username = username;
      }
      public String getPassword() {
          return password;
      }
      public void setPassword(String password) {
          this.password = password;
      }
      public String getNickname() {
          return nickname;
      }
      public void setNickname(String nickname) {
          this.nickname = nickname;
      }
      public Date getRegdate() {
          return regdate;
      }
      public void setRegdate(Date regdate) {
          this.regdate = regdate;
      }
      public int getLevel() {
          return level;
      }
      public void setLevel(int level) {
          this.level = level;
      }
      @Override
      public String toString() {
          return "User [id=" + id + ", username=" + username + ", password=" + password + ", nickname=" + nickname
                  + ", regdate=" + regdate + ", level=" + level + "]";
      }
      public User() {}
  
      public User(int id, String username, String password, String nickname, Date regdate, int level) {
  
          this.id = id;
          this.username = username;
          this.password = password;
          this.nickname = nickname;
          this.regdate = regdate;
          this.level = level;
      }
  
  
  
  
  }
  
  ```

  

- DAO层-SQL映射文件(mapper.xml)

  ```userM.xml
  <?xml version="1.0" encoding="utf8"?>
  <!DOCTYPE mapper
          PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
          "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
  <mapper namespace="com.yt.dao.UserDao">
      <select id="getAllUsers" resultType="com.yt.entity.User">
          select * from user
      </select>
  </mapper>
  ```

  

- 创建测试类

  - 读取核心配置文件mybatis-config.xml

  - 创建SqlSessionFactory对象，读取配置文件

  - 创建SqlSession对象

    ```MyBatisUtil.java
    
    public class MyBatisUtil {
    	private static SqlSessionFactory factory;
    	
    	static{ 
    		System.out.println("static factory===============");
    		try {
    			InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
    			factory = new SqlSessionFactoryBuilder().build(is);
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} 
    	}
    	
    	public static SqlSession createSqlSession(){
    		return factory.openSession(false); 
    	}
    	
    	public static void closeSqlSession(SqlSession sqlSession){
    		if(null != sqlSession) 
    			sqlSession.close();
    	}
    }
    ```

  - 调用mapper文件进行数据操作

  ```Test.java
  public class Test {
      public static void main(String[] args) {
          SqlSession session = MyBatisUtil.createSqlSession();
  		List<User> userlist=session.getMapper(UserDao.class).getAllUsers();
  		for(User u:userlist) {
  			System.out.println(u);
  		}
          MyBatisUtil.closeSqlSession(session);
  
      }
  }
  
  ```

  

## 查询

### 模糊查询

只有一个参数的时候，可以随便叫什么

```

<!--    concat () 用于连接字符串  % 是任意多个任意字符  _ 是一个任意字符 -->
<!--    #{}  用于取值  参数-->
    <select id="getUsersByName" resultType="u">
        select * from user where username like CONCAT ('%',#{username},'%')
        or nickname like CONCAT ('%',#{username},'%')
    </select>
```

多个参数时，复杂数据类型

Java实体类、Map等，通过#{属性名}或者#{map的keyName}即可获取传入值